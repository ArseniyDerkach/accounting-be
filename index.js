import express from "express";
import cors from "cors";
import jwt from "jsonwebtoken";
import { usersArray, dataArray } from "./mock.js";

const JWT_SECRET =
  "be274d4013154ea789846763c119fc8492b702446e4f265b50e628e494918d23";

const app = express();

const verifyToken = (
  req,
  res,
  next
) => {
  const authHeader = req.headers.authorization;
  if (!authHeader) {
    return res.status(401).json({ message: "Unauthorized" });
  }

  const token = authHeader.split(" ")[1];
  if (!token) {
    return res.status(401).json({ message: "Unauthorized" });
  }

  try {
    const decoded = jwt.verify(token, JWT_SECRET);
    req.user = decoded;
    next();
  } catch (err) {
    return res.status(401).json({ message: "Unauthorized" });
  }
};

app.use(cors());
app.use(express.json());

const port = 8080;

app.use((req, res, next) => {
  if (req.path === "/login") {
    next();
  } else {
    verifyToken(req, res, next);
  }
});

app.post("/login", (req, res) => {
  const { name } = req.body;
  const token = jwt.sign({ name }, JWT_SECRET);
  const id = Date.now();
  res.json({ message: "Logged in successfully", token, name, id });
  const user = {
    id,
    name,
  };
  usersArray.push(user);
});

// app.get("/", function (req, res) {
//   res.send("lorem ipsum12345");
// });

// app.get("/rows", function (req, res) {
//   res.status(401).send("posts not found");
// });

app.get("/rows", async function (req, res) {
  // const data = await getUsers();

  res.send(JSON.stringify(dataArray));
});

app.post("/row", async function (req, res) {
  const row = req.body;
  dataArray.push(row);
  res.send(row);
});

app.delete("/row", async function(req, res) {
  const id = req.body;
  const rowToDelete = dataArray.findIndex(item => item.id === id);
  const deletedItem = dataArray.splice(rowToDelete, 1);
  res.send(deletedItem);
});

app.listen(port, () => {
  console.log(`server started`);
});
