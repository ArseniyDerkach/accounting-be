const { MongoClient, ServerApiVersion } = require("mongodb");
const uri =
  "mongodb+srv://arseniiderkach:bPEjcYr1ZYyNuEzj@cluster0.lfxlcmo.mongodb.net/?retryWrites=true&w=majority";
// Create a MongoClient with a MongoClientOptions object to set the Stable API version
const client = new MongoClient(uri, {
  serverApi: {
    version: ServerApiVersion.v1,
    strict: true,
    deprecationErrors: true,
  },
});
async function run() {
  try {
    await client.connect();
    const db = await client.db("userlist");
    const usersCollection = await db.collection("users");
    const data = {
      id: Date.now(),
      firstName: "Jack",
      lastName: "Daniels",
      age: 35,
    };
  } finally {
    // Ensures that the client will close when you finish/error
    await client.close();
  }
}
// run().catch(console.dir);

async function getUsers() {
  await client.connect();
  const db = await client.db("userlist");
  const usersCollection = await db.collection("users");
  const result = await usersCollection.find({}).toArray();
  await client.close();
  return result;
}
async function addUser(user) {
  await client.connect();
  const db = await client.db("userlist");
  const usersCollection = await db.collection("users");
  const newUser = await usersCollection.insertOne(user);
  await client.close();
  return newUser;
}

// async function login(loginInfo) {
//   await client.connect();
//   const db = await client.db('accounting');
//   const usersCollection = await db.collection("users");
//   const currentUser = 
// }
// TODO: винести код з 77-79 та 81 рядку в окремий метод(?)
// TODO: спробувати це написати через метод, який буде приймати колбек-функцію
exports.getUsers = getUsers;
exports.addUser = addUser;
